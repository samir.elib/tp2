package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private LiveData<List<Book>> allBooks;

    private MutableLiveData<List<Book>> selectedBook =
            new MutableLiveData<>();

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public void insertbook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }

    public void updatebook(Book update) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(update);
        });
    }

    public void deletebook(long bookId) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(bookId);
        }); }


    public void getBooks ( long id){
                Future<List<Book>> sbooks = databaseWriteExecutor.submit(() -> {
                    return bookDao.getBook(id);
                });
                try {
                    selectedBook.setValue(sbooks.get());
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<List<Book>> getSearchResults() {
        return selectedBook;
    }


}

