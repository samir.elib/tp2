package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.BookRepository;
import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel
{
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<List<Book>> searchResults;

    public ListViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
        searchResults = repository.getSearchResults();
    }

    MutableLiveData<List<Book>> getSearchResults() {
        return searchResults;
    }
    public LiveData<List<Book>> getAllbooks() {
        return allBooks;
    }

    public void insertBook(Book book) {
        repository.insertbook(book);
    }

    public void getBook(long id) {
        repository.getBooks(id);
    }

    public void deleteBook(long id) {
        repository.deletebook(id);
    }


}
